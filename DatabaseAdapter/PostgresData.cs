﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;


namespace DatabaseAdapter
{
    public class PostgresData : IDatabase
    {
        // forced by IDatabase
        private string host, port, username, password, databaseName;
        private bool connected;
        public bool Connected { get { return this.connected; } }
        public string ConnectionString
        {
            get
            {
                return "Host=" + this.host + ";Port=" + this.port + ";Username=" + this.username + ";Password=" + this.password + ";Database=" + this.databaseName;
            }
        }

        // internal
        private NpgsqlConnection connection;

        public PostgresData(string host, string port, string username, string password, string databaseName)
        {
            this.host = host;
            this.port = port;
            this.username = username;
            this.password = password;
            this.databaseName = databaseName;
            this.connected = false;
        }


        public bool Connect()
        {
            this.connection = new NpgsqlConnection(this.ConnectionString);
            this.connection.Open();
            this.connected = true;
            return this.connected;
        }

        public void Disconnect()
        {
            this.connection.Close();
            this.connected = false;
        }

        public DataSet ExecuteSqlQuery(string query)
        {
            DataSet result;

            using (var resultTable = new NpgsqlDataAdapter(query, this.connection))
            {
                result = new DataSet();
                resultTable.Fill(result);
            }                 
            return result;
        }

        public bool TestConnection()
        {
            throw new NotImplementedException();
        }



    }
}
