﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DatabaseAdapter
{
    public interface IDatabase
    {
        bool Connected { get; }

        string ConnectionString { get; }

        bool Connect();

        bool TestConnection();

        DataSet ExecuteSqlQuery(string query);

        void Disconnect();
    }
}
