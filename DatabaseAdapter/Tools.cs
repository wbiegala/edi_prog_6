﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Xml.Serialization;
using Excel = Microsoft.Office.Interop.Excel;


namespace DatabaseAdapter
{
    public static class Tools
    {

        public static void SaveAsCsv(DataTable table, Stream stream)
        {
            using (StreamWriter sw = new StreamWriter(stream))
            {
                string header = "";

                foreach (DataColumn name in table.Columns)
                {
                    header += name.ColumnName + ";";
                }

                header = header.Remove(header.Length - 1);

                sw.WriteLine(header);

                foreach (DataRow row in table.Rows)
                {
                    string line = "";

                    foreach (var field in row.ItemArray)
                    {
                        line += field.ToString() + ";";
                    }
                    line = line.Remove(line.Length - 1);

                    sw.WriteLine(line);
                }
            }
        }



        public static void SaveAsXlsx(DataTable table, string file)
        {
            var excelApp = new Excel.Application();
            excelApp.Workbooks.Add();

            Excel._Worksheet workSheet = excelApp.ActiveSheet;

            for (var i = 0; i < table.Columns.Count; i++)
            {
                workSheet.Cells[1, i + 1] = table.Columns[i].ColumnName;
            }

            for (var i = 0; i < table.Rows.Count; i++)
            {
                for (var j = 0; j < table.Columns.Count; j++)
                {
                    workSheet.Cells[i + 2, j + 1] = table.Rows[i][j];
                }
            }
            workSheet.SaveAs(file);
            excelApp.Quit();
        }


        public static void SaveAsXml(DataTable table, string file)
        {
            string xml = "";

            using (MemoryStream ms = new MemoryStream())
            {
                using (TextWriter sw = new StreamWriter(ms))
                {
                    var xmlSerializer = new XmlSerializer(typeof(DataTable));
                    xmlSerializer.Serialize(sw, table);
                    xml = Encoding.UTF8.GetString(ms.ToArray());
                }
            }

            using (FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.Write(xml);
                }
            }

        }




    }
}
