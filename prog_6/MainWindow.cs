﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prog_6
{
    public partial class MainWindow : Form
    {
        DatabaseAdapter.IDatabase database;
        DataSet resultOfSQL;

        public MainWindow()
        {
            InitializeComponent();
            this.disconnectButton.Visible = false;
            this.resultGroupBox.Enabled = false;
            this.queryGroupBox.Enabled = false;            
        }

        private void OracleRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.hostTextBox.Clear();
            this.portTextBox.Clear();
            this.usernameTextBox.Clear();
            this.passwordTextBox.Clear();
            this.dbnameTextBox.Clear();

            if (this.OracleRadioButton.Checked == true)
            {
                this.dbnameLabel.Text = "SID";
                this.portTextBox.Text = "1521";
            }
            else if (this.postgresRadioButton.Checked == true)
            {
                this.dbnameLabel.Text = "Baza danych";
                this.portTextBox.Text = "5432";
            }
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            DatabaseAdapter.IDatabase toTest;

            if (this.OracleRadioButton.Checked == true)
            {
                toTest = new DatabaseAdapter.OracleData(hostTextBox.Text, portTextBox.Text, usernameTextBox.Text, passwordTextBox.Text, dbnameTextBox.Text);
            }
            else if (this.postgresRadioButton.Checked == true)
            {
                toTest = new DatabaseAdapter.PostgresData(hostTextBox.Text, portTextBox.Text, usernameTextBox.Text, passwordTextBox.Text, dbnameTextBox.Text);
            }
            else throw new Exception();

            try
            {
                if (toTest.TestConnection())
                {
                    MessageBox.Show("Połączenie prawidłowe.", "Test połączenia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd połączenia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (this.OracleRadioButton.Checked == true)
            {
                database = new DatabaseAdapter.OracleData(hostTextBox.Text, portTextBox.Text, usernameTextBox.Text, passwordTextBox.Text, dbnameTextBox.Text);
            }
            else if (this.postgresRadioButton.Checked == true)
            {
                database = new DatabaseAdapter.PostgresData(hostTextBox.Text, portTextBox.Text, usernameTextBox.Text, passwordTextBox.Text, dbnameTextBox.Text);
            }
            else throw new Exception();


            try
            {
                if (database.Connect())
                {
                    this.rdbmsGroupBox.Enabled = false;
                    this.disconnectButton.Visible = true;
                    this.queryGroupBox.Enabled = true;
                    this.statusLabel.ForeColor = Color.ForestGreen;
                    this.statusLabel.Text = "Połączony";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd połączenia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            this.disconnect();
        }


        private void disconnect()
        {
            this.database.Disconnect();
            this.database = null;
            this.statusLabel.ForeColor = Color.Red;
            this.statusLabel.Text = "Rozłączony";
            this.SQLresultDataGridView.DataSource = null;
            this.sqlTextBox.Clear();
            this.rdbmsGroupBox.Enabled = true;
            this.disconnectButton.Visible = false;
            this.resultGroupBox.Enabled = false;
            this.queryGroupBox.Enabled = false;
        }

        private void executeOrder66Button_Click(object sender, EventArgs e)
        {

            this.sqlTextBox.Text = this.sqlTextBox.Text.TrimStart(' ', '\t', '\n');
            string[] temp = this.sqlTextBox.Text.Split(' ');

            if (temp[0].ToUpper() == "SELECT")
            {

                try
                {
                    this.SQLresultDataGridView.DataSource = null;
                    this.resultGroupBox.Enabled = true;
                    this.SQLresultDataGridView.AutoGenerateColumns = true;
                    this.resultOfSQL = this.database.ExecuteSqlQuery(this.sqlTextBox.Text);
                    this.SQLresultDataGridView.DataSource = resultOfSQL.Tables[0];
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Zapytanie musi zaczynać się od słowa kluczowego SELECT", "Nieprawidłowa komenda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        ~MainWindow()
        {
            this.database.Disconnect();
        }

        private void csvButton_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveAsCsv = new SaveFileDialog())
            {
                saveAsCsv.Filter = "Plik .csv | *.csv";
                saveAsCsv.FilterIndex = 0;
                saveAsCsv.RestoreDirectory = true;

                if (saveAsCsv.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        DatabaseAdapter.Tools.SaveAsCsv(this.resultOfSQL.Tables[0], saveAsCsv.OpenFile());
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }


        }

        private void xlsxButton_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveAsXlsx = new SaveFileDialog())
            {
                saveAsXlsx.Filter = "Plik Microsoft Excel | *.xlsx";
                saveAsXlsx.FilterIndex = 0;
                saveAsXlsx.RestoreDirectory = true;

                if (saveAsXlsx.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        DatabaseAdapter.Tools.SaveAsXlsx(this.resultOfSQL.Tables[0], saveAsXlsx.FileName);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
