﻿namespace prog_6
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdbmsGroupBox = new System.Windows.Forms.GroupBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.dbnameLabel = new System.Windows.Forms.Label();
            this.dbnameTextBox = new System.Windows.Forms.TextBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.hostLabel = new System.Windows.Forms.Label();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.portLabel = new System.Windows.Forms.Label();
            this.hostTextBox = new System.Windows.Forms.TextBox();
            this.postgresRadioButton = new System.Windows.Forms.RadioButton();
            this.OracleRadioButton = new System.Windows.Forms.RadioButton();
            this.dbstatusGroupBox = new System.Windows.Forms.GroupBox();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.statusLabel = new System.Windows.Forms.Label();
            this.queryGroupBox = new System.Windows.Forms.GroupBox();
            this.executeOrder66Button = new System.Windows.Forms.Button();
            this.sqlTextBox = new System.Windows.Forms.TextBox();
            this.resultGroupBox = new System.Windows.Forms.GroupBox();
            this.SQLresultDataGridView = new System.Windows.Forms.DataGridView();
            this.csvButton = new System.Windows.Forms.Button();
            this.xlsxButton = new System.Windows.Forms.Button();
            this.rdbmsGroupBox.SuspendLayout();
            this.dbstatusGroupBox.SuspendLayout();
            this.queryGroupBox.SuspendLayout();
            this.resultGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SQLresultDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // rdbmsGroupBox
            // 
            this.rdbmsGroupBox.Controls.Add(this.connectButton);
            this.rdbmsGroupBox.Controls.Add(this.dbnameLabel);
            this.rdbmsGroupBox.Controls.Add(this.dbnameTextBox);
            this.rdbmsGroupBox.Controls.Add(this.passwordLabel);
            this.rdbmsGroupBox.Controls.Add(this.passwordTextBox);
            this.rdbmsGroupBox.Controls.Add(this.usernameLabel);
            this.rdbmsGroupBox.Controls.Add(this.usernameTextBox);
            this.rdbmsGroupBox.Controls.Add(this.hostLabel);
            this.rdbmsGroupBox.Controls.Add(this.portTextBox);
            this.rdbmsGroupBox.Controls.Add(this.portLabel);
            this.rdbmsGroupBox.Controls.Add(this.hostTextBox);
            this.rdbmsGroupBox.Controls.Add(this.postgresRadioButton);
            this.rdbmsGroupBox.Controls.Add(this.OracleRadioButton);
            this.rdbmsGroupBox.Location = new System.Drawing.Point(13, 13);
            this.rdbmsGroupBox.Name = "rdbmsGroupBox";
            this.rdbmsGroupBox.Size = new System.Drawing.Size(329, 241);
            this.rdbmsGroupBox.TabIndex = 0;
            this.rdbmsGroupBox.TabStop = false;
            this.rdbmsGroupBox.Text = "Ustawienia bazy danych";
            // 
            // connectButton
            // 
            this.connectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.connectButton.Location = new System.Drawing.Point(227, 212);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(95, 23);
            this.connectButton.TabIndex = 12;
            this.connectButton.Text = "Połącz";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // dbnameLabel
            // 
            this.dbnameLabel.AutoSize = true;
            this.dbnameLabel.Location = new System.Drawing.Point(6, 159);
            this.dbnameLabel.Name = "dbnameLabel";
            this.dbnameLabel.Size = new System.Drawing.Size(69, 13);
            this.dbnameLabel.TabIndex = 11;
            this.dbnameLabel.Text = "Baza danych";
            // 
            // dbnameTextBox
            // 
            this.dbnameTextBox.Location = new System.Drawing.Point(6, 175);
            this.dbnameTextBox.Name = "dbnameTextBox";
            this.dbnameTextBox.Size = new System.Drawing.Size(317, 20);
            this.dbnameTextBox.TabIndex = 10;
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Location = new System.Drawing.Point(168, 117);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(36, 13);
            this.passwordLabel.TabIndex = 9;
            this.passwordLabel.Text = "Hasło";
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(171, 133);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(152, 20);
            this.passwordTextBox.TabIndex = 8;
            this.passwordTextBox.UseSystemPasswordChar = true;
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Location = new System.Drawing.Point(3, 117);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(62, 13);
            this.usernameLabel.TabIndex = 7;
            this.usernameLabel.Text = "Użytkownik";
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(6, 133);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(153, 20);
            this.usernameTextBox.TabIndex = 6;
            // 
            // hostLabel
            // 
            this.hostLabel.AutoSize = true;
            this.hostLabel.Location = new System.Drawing.Point(4, 74);
            this.hostLabel.Name = "hostLabel";
            this.hostLabel.Size = new System.Drawing.Size(29, 13);
            this.hostLabel.TabIndex = 5;
            this.hostLabel.Text = "Host";
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(227, 90);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(96, 20);
            this.portTextBox.TabIndex = 4;
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Location = new System.Drawing.Point(224, 74);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(26, 13);
            this.portLabel.TabIndex = 3;
            this.portLabel.Text = "Port";
            // 
            // hostTextBox
            // 
            this.hostTextBox.Location = new System.Drawing.Point(6, 90);
            this.hostTextBox.Name = "hostTextBox";
            this.hostTextBox.Size = new System.Drawing.Size(215, 20);
            this.hostTextBox.TabIndex = 2;
            // 
            // postgresRadioButton
            // 
            this.postgresRadioButton.AutoSize = true;
            this.postgresRadioButton.Location = new System.Drawing.Point(7, 44);
            this.postgresRadioButton.Name = "postgresRadioButton";
            this.postgresRadioButton.Size = new System.Drawing.Size(66, 17);
            this.postgresRadioButton.TabIndex = 1;
            this.postgresRadioButton.TabStop = true;
            this.postgresRadioButton.Text = "Postgres";
            this.postgresRadioButton.UseVisualStyleBackColor = true;
            // 
            // OracleRadioButton
            // 
            this.OracleRadioButton.AutoSize = true;
            this.OracleRadioButton.Location = new System.Drawing.Point(7, 20);
            this.OracleRadioButton.Name = "OracleRadioButton";
            this.OracleRadioButton.Size = new System.Drawing.Size(56, 17);
            this.OracleRadioButton.TabIndex = 0;
            this.OracleRadioButton.TabStop = true;
            this.OracleRadioButton.Text = "Oracle";
            this.OracleRadioButton.UseVisualStyleBackColor = true;
            this.OracleRadioButton.CheckedChanged += new System.EventHandler(this.OracleRadioButton_CheckedChanged);
            // 
            // dbstatusGroupBox
            // 
            this.dbstatusGroupBox.Controls.Add(this.disconnectButton);
            this.dbstatusGroupBox.Controls.Add(this.statusLabel);
            this.dbstatusGroupBox.Location = new System.Drawing.Point(349, 13);
            this.dbstatusGroupBox.Name = "dbstatusGroupBox";
            this.dbstatusGroupBox.Size = new System.Drawing.Size(469, 61);
            this.dbstatusGroupBox.TabIndex = 1;
            this.dbstatusGroupBox.TabStop = false;
            this.dbstatusGroupBox.Text = "Status";
            // 
            // disconnectButton
            // 
            this.disconnectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.disconnectButton.Location = new System.Drawing.Point(377, 11);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(86, 44);
            this.disconnectButton.TabIndex = 1;
            this.disconnectButton.Text = "Rozłącz";
            this.disconnectButton.UseVisualStyleBackColor = true;
            this.disconnectButton.Click += new System.EventHandler(this.disconnectButton_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.statusLabel.ForeColor = System.Drawing.Color.Red;
            this.statusLabel.Location = new System.Drawing.Point(6, 20);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(198, 37);
            this.statusLabel.TabIndex = 0;
            this.statusLabel.Text = "Rozłączony";
            // 
            // queryGroupBox
            // 
            this.queryGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.queryGroupBox.Controls.Add(this.executeOrder66Button);
            this.queryGroupBox.Controls.Add(this.sqlTextBox);
            this.queryGroupBox.Location = new System.Drawing.Point(349, 81);
            this.queryGroupBox.Name = "queryGroupBox";
            this.queryGroupBox.Size = new System.Drawing.Size(469, 173);
            this.queryGroupBox.TabIndex = 2;
            this.queryGroupBox.TabStop = false;
            this.queryGroupBox.Text = "Zapytanie";
            // 
            // executeOrder66Button
            // 
            this.executeOrder66Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.executeOrder66Button.Location = new System.Drawing.Point(7, 144);
            this.executeOrder66Button.Name = "executeOrder66Button";
            this.executeOrder66Button.Size = new System.Drawing.Size(456, 23);
            this.executeOrder66Button.TabIndex = 1;
            this.executeOrder66Button.Text = "Wykonaj zapytanie";
            this.executeOrder66Button.UseVisualStyleBackColor = true;
            this.executeOrder66Button.Click += new System.EventHandler(this.executeOrder66Button_Click);
            // 
            // sqlTextBox
            // 
            this.sqlTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sqlTextBox.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sqlTextBox.Location = new System.Drawing.Point(7, 20);
            this.sqlTextBox.Multiline = true;
            this.sqlTextBox.Name = "sqlTextBox";
            this.sqlTextBox.Size = new System.Drawing.Size(456, 118);
            this.sqlTextBox.TabIndex = 0;
            // 
            // resultGroupBox
            // 
            this.resultGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultGroupBox.Controls.Add(this.SQLresultDataGridView);
            this.resultGroupBox.Controls.Add(this.csvButton);
            this.resultGroupBox.Controls.Add(this.xlsxButton);
            this.resultGroupBox.Location = new System.Drawing.Point(13, 261);
            this.resultGroupBox.Name = "resultGroupBox";
            this.resultGroupBox.Size = new System.Drawing.Size(805, 350);
            this.resultGroupBox.TabIndex = 3;
            this.resultGroupBox.TabStop = false;
            this.resultGroupBox.Text = "Wynik";
            // 
            // SQLresultDataGridView
            // 
            this.SQLresultDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SQLresultDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SQLresultDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.SQLresultDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SQLresultDataGridView.Location = new System.Drawing.Point(7, 20);
            this.SQLresultDataGridView.MultiSelect = false;
            this.SQLresultDataGridView.Name = "SQLresultDataGridView";
            this.SQLresultDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SQLresultDataGridView.Size = new System.Drawing.Size(791, 295);
            this.SQLresultDataGridView.TabIndex = 2;
            // 
            // csvButton
            // 
            this.csvButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.csvButton.Location = new System.Drawing.Point(500, 321);
            this.csvButton.Name = "csvButton";
            this.csvButton.Size = new System.Drawing.Size(146, 23);
            this.csvButton.TabIndex = 1;
            this.csvButton.Text = "Eksportuj do Excel (.csv)";
            this.csvButton.UseVisualStyleBackColor = true;
            this.csvButton.Click += new System.EventHandler(this.csvButton_Click);
            // 
            // xlsxButton
            // 
            this.xlsxButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.xlsxButton.Location = new System.Drawing.Point(652, 321);
            this.xlsxButton.Name = "xlsxButton";
            this.xlsxButton.Size = new System.Drawing.Size(146, 23);
            this.xlsxButton.TabIndex = 0;
            this.xlsxButton.Text = "Eksportuj do Excel (.xlsx)";
            this.xlsxButton.UseVisualStyleBackColor = true;
            this.xlsxButton.Click += new System.EventHandler(this.xlsxButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 623);
            this.Controls.Add(this.resultGroupBox);
            this.Controls.Add(this.queryGroupBox);
            this.Controls.Add(this.dbstatusGroupBox);
            this.Controls.Add(this.rdbmsGroupBox);
            this.Name = "MainWindow";
            this.Text = "Zapytawacz";
            this.rdbmsGroupBox.ResumeLayout(false);
            this.rdbmsGroupBox.PerformLayout();
            this.dbstatusGroupBox.ResumeLayout(false);
            this.dbstatusGroupBox.PerformLayout();
            this.queryGroupBox.ResumeLayout(false);
            this.queryGroupBox.PerformLayout();
            this.resultGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SQLresultDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox rdbmsGroupBox;
        private System.Windows.Forms.Label hostLabel;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.TextBox hostTextBox;
        private System.Windows.Forms.RadioButton postgresRadioButton;
        private System.Windows.Forms.RadioButton OracleRadioButton;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.Label dbnameLabel;
        private System.Windows.Forms.TextBox dbnameTextBox;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.GroupBox dbstatusGroupBox;
        private System.Windows.Forms.GroupBox queryGroupBox;
        private System.Windows.Forms.Button executeOrder66Button;
        private System.Windows.Forms.TextBox sqlTextBox;
        private System.Windows.Forms.GroupBox resultGroupBox;
        private System.Windows.Forms.DataGridView SQLresultDataGridView;
        private System.Windows.Forms.Button csvButton;
        private System.Windows.Forms.Button xlsxButton;
        private System.Windows.Forms.Button disconnectButton;
        private System.Windows.Forms.Label statusLabel;
    }
}

